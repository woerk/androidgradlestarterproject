Vi kan bygge en Android app med dependencies fra command line med gradle. 

Formål: Få dependencies inkluderet i projektet med det samme. Have standard projekter/templates -kunne bygge projektet igen ved ændringer vi ikke ønsker. Arbejde mod mere automation.

Prerequisites: Download Android SDK og gradle* og sæt PATH environement variables. 

PATH Environment variables skal være defineret for Android SDK, SDK/tools, SDK/platform-tools, 
ANDROID_HOME og gradle. 

Eksempel herpå: 

1) kør kommando: open /Applications/TextEdit.app/ ~/.bash_profile 

2) Indsæt noget der ligner (afhængig af hvor Android SDK, gradle er installeret osv.) følgende: 
-Android – kan fx ligge her: 
#export ANDROID_HOME=/Applications/adt-bundle-mac-x86_64-20131030/sdk/ 
#export ANDROID_HOME=/Applications/Android\ Studio.app/sdk/ 
#export PATH=/Applications/Android\ Studio.app/sdk/tools:/Applications/Android\ Studio.app/sdk/platform-tools/:$PATH 
- gradle kan fx ligge her i version 1.10 
#export PATH="~/GRADLE_HOME/gradle-1.10/bin:$PATH" 
-evt. Også Java export JAVA_HOME=$(/usr/libexec/java_home) 

3) luk terminalen og åben den igen eller genindlæs ~/bash_profile ved at køre kommando'en: source ~/.bash_profile 

4) tjek om PATH environement variables er sat. Kør kommando: echo $PATH Hvis du får printet noget der ligner:
 ~/GRADLE_HOME/gradle-1.10/bin:/Applications/Android Studio.app/sdk/tools:/Applications/Android Studio.app/sdk/platform-tools/ 
 så virker det. Ellers læs en bog om UNIX eller Linux eller spørg en eller anden. 
 
 5) Clone dette android gradle starter project. 
 
 6) stil dig i roden af projektet. Kør kommando'en: “gradle build”. Eller “gradle clean build” 
 
 7) Åben projektet i Android Studio. Vælg “build.gradle”. Vælg OK til den skærm der popper op. Kør projektet. 
 
 
 
 Todo 
 - Prøv at hente Eclipse plugin og se om det spiller med Eclipse. 
 - Prøv at se om gradle wrapper gør at man ikke behøver at have gradle installeret for at kunne køre projektet.
 - Lav et script. Bl.a. så vi kan lave projekter der hedder noget andet end “anotherWoerkProject” til at starte med.  
 - Lav nogle script/parameter der gør at vi bl.a. kan kreerer Activities når projectet laves. 
 - Lave flere scripts. 
 
 
 
 Info 
 Libs bliver ikke bundlet, men ligger her: ~/.gradle/caches/modules-2/files-2.1/<name-of-the-lib>/
 <name-of-the-lib>/<version-no>/<hash-value>/<the-actual-jar-file> 
 Det er Maven style. Gradle supportere Maven og Ivy for dependencies. 
 
 For at generere Eclipse filer, kør kommando: #gradle eclipse".
 
 *hvis gradle ikke er installeret så skulle man kunne brug gradle wrapper i stedet. 
 Har ikke testet det endnu. 
 kør kommando: gradle wrapper 
 kør kommando (i stedet for gradle build): ./gradlew build 
 
 
 Ved brug af Eclipse kan man installerer et gradle plugin: URL'en for plugin kan være: 
 http://dist.springsource.com/release/TOOLS/gradle. Vælg gradle IDE. 
 Se evt. https://github.com/spring-projects/eclipse-integration-gradle